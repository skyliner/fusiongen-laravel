<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

/**
 * Class CheckForInstallation.
 */
class CheckForInstallation
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_INSTALLED', false) === false && $request->path() != 'install') {
            return redirect()->route('frontend.install');
        }

        return $next($request);
    }
}
